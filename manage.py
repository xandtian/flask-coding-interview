from flask_migrate import Migrate

from app import create_app
from db import db

app = create_app()

db.init_app(app)
migrate = Migrate(app, db)
db.create_all()

if __name__ == '__main__':
    app.run()
