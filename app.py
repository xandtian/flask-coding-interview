from flask import Flask

from apps.students.routes import student_app


def create_app(**config_overrides):
    app = Flask(__name__)

    # apply overrides
    app.config.update(config_overrides)

    app.config['SECRET_KEY'] = ''

    # setup db
    app.config['SQLALCHEMY_DATABASE_URI'] = f""

    from apps.courses.routes import courses_app
    from apps.main.routes import main_app
    from apps.users.routes import user_app

    # register blueprints
    app.register_blueprint(main_app)
    app.register_blueprint(user_app)
    app.register_blueprint(courses_app)
    app.register_blueprint(student_app)
    return app
