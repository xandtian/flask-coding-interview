from flask import Blueprint, request

from apps.students.models import Student
from db import db

student_app = Blueprint("student_app", __name__)


class InvalidStudentError(Exception):
    pass


@student_app.route("/api/v1/student", methods=("POST",))
def create_student():
    r = request.json()
    try:
        student = Student(
            r.first_name,
            r.last_name,
            r.email,
            r.password,
            r.is_active,
            r.enroll_complete,
        )
    except:
        raise InvalidStudentError

    return 'OK'

@student_app.route("/api/v1/student", methods=("GET",))
def get_student():
    students = db.session.query(Student).all()
    return students
