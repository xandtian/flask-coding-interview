from datetime import datetime

from sqlalchemy import Column

from db import db


# ToDo: Create Users Model
class Student(db.Model):
    id = Column(db.Integer, primary_key=True)
    first_name = Column(db.String(50), nullable=False)
    last_name = Column(db.String(50), nullable=False)
    birth_date = Column(db.DateTime(), nullable=False)
    email = Column(db.String(50), nullable=False)
    password = Column(db.String(50), nullable=False) #TODO: secure
    is_active = Column(db.Boolean(1), nullable=False, default=True)
    enroll_complete = Column(db.Boolean(1), nullable=False, default=True)

    def __init(self, first_name, last_name, birth_date, email, password, is_active, enroll_date):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date
        self.email = email
        self.password = password
        self.is_active = is_active
        self.enroll_complete = enroll_date


    def __repr__(self):
        return self.first_name, self.email
