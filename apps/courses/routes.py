from flask import Blueprint


courses_app = Blueprint("courses_app", __name__)


@courses_app.route("/api/v1/courses", methods=("GET",))
def courses():
    return "OK Courses"
